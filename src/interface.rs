use std::ffi::CString;
use std::net::Ipv4Addr;

use libc::c_char;

extern "C" {
    fn interface_set_address(dev: *const c_char, address: u32) -> i32;
    fn interface_get_address(dev: *const c_char, address: *mut u32) -> i32;
    fn interface_set_netmask(dev: *const c_char, mask: u32) -> i32;
    fn interface_set_mtu(dev: *const c_char, mtu: i32) -> i32;
    fn interface_set_status(dev: *const c_char, status: i32) -> i32;
}

pub enum InterfaceStatus {
    Down = 0,
    Up = 1,
}

pub fn set_address(dev: &str, address: Ipv4Addr) -> std::io::Result<()> {
    let dev_cstr = CString::new(dev).unwrap();
    let result = unsafe { interface_set_address(dev_cstr.as_ptr(), u32::from(address).to_be()) };
    if result < 0 {
        return Err(std::io::Error::last_os_error());
    }
    Ok(())
}

pub fn get_address(dev: &str) -> std::io::Result<Ipv4Addr> {
    let dev_cstr = CString::new(dev).unwrap();
    let mut address = 0;
    let result = unsafe { interface_get_address(dev_cstr.as_ptr(), &mut address) };
    if result < 0 {
        return Err(std::io::Error::last_os_error());
    }
    Ok(Ipv4Addr::from(address.to_be()))
}

pub fn set_netmask(dev: &str, netmask: Ipv4Addr) -> std::io::Result<()> {
    let dev_cstr = CString::new(dev).unwrap();
    let result = unsafe { interface_set_netmask(dev_cstr.as_ptr(), u32::from(netmask).to_be()) };
    if result < 0 {
        return Err(std::io::Error::last_os_error());
    }
    Ok(())
}

pub fn set_mtu(dev: &str, mtu: u32) -> std::io::Result<()> {
    let dev_cstr = CString::new(dev).unwrap();
    let result = unsafe { interface_set_mtu(dev_cstr.as_ptr(), mtu as i32) };
    if result < 0 {
        return Err(std::io::Error::last_os_error());
    }
    Ok(())
}

pub fn set_status(dev: &str, status: InterfaceStatus) -> std::io::Result<()> {
    let dev_cstr = CString::new(dev).unwrap();
    let result = unsafe { interface_set_status(dev_cstr.as_ptr(), status as i32) };
    if result < 0 {
        return Err(std::io::Error::last_os_error());
    }
    Ok(())
}
