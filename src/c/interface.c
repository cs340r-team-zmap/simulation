#include <arpa/inet.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "util.h"

static struct ifreq
prepare_ifreq (const char *dev)
{
  struct ifreq ifr;
  memset (&ifr, 0, sizeof (ifr));

  strncpy (ifr.ifr_name, dev, IFNAMSIZ);
  ifr.ifr_name[IFNAMSIZ - 1] = '\0';

  return ifr;
}

int
interface_set_address (const char *dev, uint32_t address)
{
  int fd;
  struct ifreq ifr;
  struct sockaddr_in *addr;

  ifr = prepare_ifreq (dev);
  addr = (struct sockaddr_in *)&ifr.ifr_addr;
  addr->sin_family = AF_INET;
  addr->sin_port = 0;
  addr->sin_addr.s_addr = address;

  if ((fd = socket (AF_INET, SOCK_DGRAM, IPPROTO_IP)) < 0)
    {
      ERROR ("Failed to create socket\n");
      return -1;
    }

  // Set the IP address for the interface.
  if (ioctl (fd, SIOCSIFADDR, (void *)&ifr) < 0)
    {
      ERROR ("Failed to set IP address for interface %s with address %d\n",
             dev, address);
      close (fd);
      return -1;
    }

  close (fd);
  return 0;
}

int
interface_get_address (const char *dev, uint32_t *address)
{
  int fd;
  struct ifreq ifr;
  struct sockaddr_in *addr;

  ifr = prepare_ifreq (dev);
  addr = (struct sockaddr_in *)&ifr.ifr_addr;
  addr->sin_family = AF_INET;

  if ((fd = socket (AF_INET, SOCK_DGRAM, IPPROTO_IP)) < 0)
    {
      ERROR ("Failed to create socket\n");
      return -1;
    }

  // Get the IP address for the interface.
  if (ioctl (fd, SIOCGIFADDR, (void *)&ifr) < 0)
    {
      ERROR ("Failed to get IP address for interface %s\n", dev);
      close (fd);
      return -1;
    }

  *address = addr->sin_addr.s_addr;
  close (fd);
  return 0;
}

int
interface_set_netmask (const char *dev, uint32_t mask)
{
  int fd;
  struct ifreq ifr;
  struct sockaddr_in *addr;

  ifr = prepare_ifreq (dev);
  addr = (struct sockaddr_in *)&ifr.ifr_addr;
  addr->sin_family = AF_INET;
  addr->sin_port = 0;
  addr->sin_addr.s_addr = mask;

  if ((fd = socket (AF_INET, SOCK_DGRAM, IPPROTO_IP)) < 0)
    {
      ERROR ("Failed to create socket\n");
      return -1;
    }

  // Set the netmask for the interface.
  if (ioctl (fd, SIOCSIFNETMASK, (void *)&ifr) < 0)
    {
      ERROR ("Failed to set netmask for interface %s with netmask %d\n", dev,
             mask);
      close (fd);
      return -1;
    }

  close (fd);
  return 0;
}

int
interface_set_mtu (const char *dev, int mtu)
{
  int fd;
  struct ifreq ifr;

  ifr = prepare_ifreq (dev);
  ifr.ifr_mtu = mtu;

  if ((fd = socket (AF_INET, SOCK_DGRAM, IPPROTO_IP)) < 0)
    {
      ERROR ("Failed to create socket\n");
      return -1;
    }

  // Set the mtu for the interface.
  if (ioctl (fd, SIOCSIFMTU, (void *)&ifr) < 0)
    {
      ERROR ("Failed to set mtu for interface %s with mtu %d\n", dev, mtu);
      close (fd);
      return -1;
    }

  close (fd);
  return 0;
}

typedef enum
{
  IF_DOWN = 0,
  IF_UP = 1,
} if_status;

int
interface_set_status (const char *dev, if_status status)
{
  int fd;
  struct ifreq ifr;

  ifr = prepare_ifreq (dev);

  if ((fd = socket (AF_INET, SOCK_DGRAM, IPPROTO_IP)) < 0)
    {
      ERROR ("Failed to create socket\n");
      return -1;
    }

  if (ioctl (fd, SIOCGIFFLAGS, (void *)&ifr) < 0)
    {
      ERROR ("Failed to get flags for interface %s\n", dev);
      close (fd);
      return -1;
    }

  if (status == IF_UP)
    ifr.ifr_flags |= (IFF_UP | IFF_RUNNING);
  else
    ifr.ifr_flags &= ~IFF_UP;

  if (ioctl (fd, SIOCSIFFLAGS, (void *)&ifr) < 0)
    {
      ERROR ("Failed to set flags for interface %s\n", dev);
      close (fd);
      return -1;
    }

  close (fd);
  return 0;
}
