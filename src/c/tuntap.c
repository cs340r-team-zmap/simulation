#include <errno.h>
#include <fcntl.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "util.h"

static char const *CLONE_DEVICE = "/dev/net/tun";

int
tun_tap_create (char *name, bool tun)
{
  int fd;
  struct ifreq ifr;

  if ((fd = open (CLONE_DEVICE, O_RDWR)) < 0)
    {
      ERROR ("Failed to open %s\n", CLONE_DEVICE);
      return -1;
    }

  memset (&ifr, 0, sizeof (ifr));
  ifr.ifr_flags = (tun ? IFF_TUN : IFF_TAP)
                  | IFF_NO_PI; // Do not include packet information.

  if (ioctl (fd, TUNSETIFF, (void *)&ifr) < 0)
    {
      ERROR ("Failed to create TUN device\n");
      close (fd);
      return -1;
    }

  // Copy the interface name to the buffer.
  strncpy (name, ifr.ifr_name, IFNAMSIZ);
  name[IFNAMSIZ - 1] = '\0';

  return fd;
}

int
tun_tap_read (int fd, void *buf, size_t len)
{
  return read (fd, buf, len);
}

int
tun_tap_write (int fd, void *buf, size_t len)
{
  return write (fd, buf, len);
}

int
tun_tap_close (int fd)
{
  return close (fd);
}
