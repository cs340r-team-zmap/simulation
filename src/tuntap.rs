use super::interface::{
    get_address, set_address, set_mtu, set_netmask, set_status, InterfaceStatus,
};
use libc::{c_char, c_void, IFNAMSIZ};
use std::ffi::CStr;
use std::io::{Read, Write};
use std::net::Ipv4Addr;
use std::os::fd::RawFd;

extern "C" {
    pub fn tun_tap_create(name: *mut c_char, tun: bool) -> i32;
    pub fn tun_tap_read(fd: i32, buf: *mut c_void, len: usize) -> i32;
    pub fn tun_tap_write(fd: i32, buf: *const c_void, len: usize) -> i32;
    pub fn tun_tap_close(fd: i32) -> i32;
}

pub struct TunTap {
    pub fd: RawFd,
    pub name: String,
}

pub struct TunTapConfig {
    pub address: Option<Ipv4Addr>,
    pub netmask: Option<Ipv4Addr>,
    pub mtu: Option<u32>,
    pub tun: bool,
}

impl TunTap {
    pub fn new(config: &TunTapConfig) -> std::io::Result<Self> {
        let mut name = [0; IFNAMSIZ];
        let fd = unsafe { tun_tap_create(name.as_mut_ptr(), config.tun) };
        if fd < 0 {
            return Err(std::io::Error::last_os_error());
        }

        let device = TunTap {
            fd,
            name: unsafe { CStr::from_ptr(name.as_ptr()).to_string_lossy().into() },
        };

        if let Some(address) = config.address {
            device.set_address(address).unwrap();
        }

        if let Some(netmask) = config.netmask {
            device.set_netmask(netmask).unwrap();
        }

        if let Some(mtu) = config.mtu {
            device.set_mtu(mtu).unwrap();
        }

        device.up().unwrap();

        Ok(device)
    }

    pub fn up(&self) -> std::io::Result<()> {
        set_status(&self.name, InterfaceStatus::Up)?;
        Ok(())
    }

    pub fn down(&self) -> std::io::Result<()> {
        set_status(&self.name, InterfaceStatus::Down)?;
        Ok(())
    }

    pub fn address(&self) -> std::io::Result<Ipv4Addr> {
        get_address(&self.name)
    }

    pub fn set_address(&self, address: Ipv4Addr) -> std::io::Result<()> {
        set_address(&self.name, address)
    }

    pub fn set_netmask(&self, netmask: Ipv4Addr) -> std::io::Result<()> {
        set_netmask(&self.name, netmask)
    }

    pub fn set_mtu(&self, mtu: u32) -> std::io::Result<()> {
        set_mtu(&self.name, mtu)?;
        Ok(())
    }
}

impl Drop for TunTap {
    fn drop(&mut self) {
        self.down().unwrap();
        unsafe { tun_tap_close(self.fd) };
    }
}

impl Read for TunTap {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        let len = unsafe { tun_tap_read(self.fd, buf.as_mut_ptr() as *mut c_void, buf.len()) };
        if len < 0 {
            return Err(std::io::Error::last_os_error());
        }
        Ok(len as usize)
    }
}

impl Write for TunTap {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        let len = unsafe { tun_tap_write(self.fd, buf.as_ptr() as *const c_void, buf.len()) };
        if len < 0 {
            return Err(std::io::Error::last_os_error());
        }
        Ok(len as usize)
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}
