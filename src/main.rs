#![allow(dead_code)]

mod interface;
mod tuntap;

use std::io::Read;

use tuntap::{TunTap, TunTapConfig};

fn main() {
    let config = TunTapConfig {
        address: None,
        netmask: None,
        mtu: None,
        tun: false,
    };

    let mut device = TunTap::new(&config).unwrap();
    loop {
        let mut buf = [0; 1504];
        println!("Reading from tap device");

        let n = device.read(&mut buf).unwrap();
        println!("Buffer: {:?}", &buf[..n]);
    }
}
