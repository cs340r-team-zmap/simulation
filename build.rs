use cc::Build;

fn main() {
    let mut builder = Build::new();

    println!("cargo::rerun-if-changed=src/c/tuntap.c");
    println!("cargo::rerun-if-changed=src/c/interface.c");
    builder
        .file("src/c/tuntap.c")
        .file("src/c/interface.c")
        .warnings(true)
        .compile("net");
}
